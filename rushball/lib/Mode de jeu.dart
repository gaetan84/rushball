import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:rushball/size.dart';
import 'package:rushball/NbrdeJoueur.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:rushball/Traduction/app_local.dart';
import 'package:rushball/Traduction/localisation.dart';
import 'package:rushball/Defilement.dart';
import 'package:rushball/config.dart';
class Modedejeu extends StatefulWidget{

  @override
  _ModedejeuState createState () => _ModedejeuState();
}
class _ModedejeuState extends State<Modedejeu>{
  @override

  Widget build (BuildContext context){
    AppLocal _local =AppLocalizations.of(context).lang;
    return new Scaffold(

      body: new Center(
        child: new Container (
            height: SizeConfig.safeBlockVertical * 66, //10 for example
            width: SizeConfig.safeBlockHorizontal * 100,
        child: GridView.count(crossAxisCount: 2,
        children: <Widget>[


          new MaterialButton(
              child: Column(
              children: <Widget>[
                new Image.asset("images/iconfinder_Resume_Bulls-eye_2316247.png",fit: BoxFit.cover,),
new Text(_local.home.mode1,style: new TextStyle(
  color: Colors.black,
  fontSize: 20,),)
              ]),

              onPressed: (){
    Navigator.push(context, new MaterialPageRoute(builder: (BuildContext context){
    return Conf(title: _local.home.mode1,);
    }));}),
          new MaterialButton(
              child: Column(
                  children: <Widget>[
                    new Image.asset("images/iconfinder_thefreeforty_chronometer_1243674.png",fit: BoxFit.cover,),
                    new Text(_local.home.mode2,style: new TextStyle(
                      color: Colors.black,
                      fontSize: 20,),),
                  ]),

              onPressed: (){
                Navigator.push(context, new MaterialPageRoute(builder: (BuildContext context){
                  return Conf(title: _local.home.mode2,);
                }));}),
          new MaterialButton(
              child: Column(
                  children: <Widget>[
                    new Image.asset("images/76252470-icône-linéaire-cible-de-pistolet-objectif-illustration-de-fine-ligne-symbole-de-contour-radar-dessin-de-c.jpg",fit: BoxFit.cover,),
                    new Text(_local.home.mode3,style: new TextStyle(
                      color: Colors.black,
                      fontSize: 20,),),
                  ]),

              onPressed: (){
                Navigator.push(context, new MaterialPageRoute(builder: (BuildContext context){
                  return Conf(title:_local.home.mode3 ,);
                }));}),
          new MaterialButton(
              child: Column(
                  children: <Widget>[
                    new Image.asset("images/iconfinder_Picture13_3289577.png",fit: BoxFit.cover,),
                    new Text(_local.home.mode4,style: new TextStyle(
                      color: Colors.black,
                      fontSize: 20,),),
                  ]),

              onPressed: (){
                Navigator.push(context, new MaterialPageRoute(builder: (BuildContext context){
                  return Conf(title: _local.home.mode4,);
                }));},shape: new RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(5.0))),
        ],),
          margin: new EdgeInsets.only(
      top: SizeConfig.safeBlockVertical *0 )
      ), ),
    );
  }
}