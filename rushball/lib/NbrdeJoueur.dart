import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:rushball/size.dart';
import 'package:rushball/config.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:rushball/Traduction/app_local.dart';
import 'package:rushball/Traduction/localisation.dart';

class NbrdeJoueur extends StatefulWidget{

  @override
  _NbrdeJoueurState createState () =>new _NbrdeJoueurState();
}

class _NbrdeJoueurState extends State<NbrdeJoueur>{

  @override

  Widget build (BuildContext context){
    AppLocal _local =AppLocalizations.of(context).lang;
    return new Scaffold(

        body: ListView(
        children: <Widget> [

        new Container(
        child: new Column(

        mainAxisAlignment: MainAxisAlignment.center,

        children: <Widget>[
          new Container(
            child:
          new Text(_local.home.nbj1,style: new TextStyle(
            color: Colors.black,
            fontSize: 20,),),
            margin: new EdgeInsets.only(
                top: SizeConfig.safeBlockVertical * 25),),
        new Container(
        height: SizeConfig.safeBlockVertical * 7, //10 for example
        width: SizeConfig.safeBlockHorizontal * 40,
        child:  new  RaisedButton ( color: Color.fromARGB(255, 0, 102, 204),elevation: 40, child:new Text(_local.home.nbj9, style: new TextStyle(
    color: Colors.white,
    fontSize: 20,),),onPressed: (){
          Navigator.of(context).push( new MaterialPageRoute(
            builder: (BuildContext context) =>
            new Conf(),
          ),   );
          },shape: new RoundedRectangleBorder(
    borderRadius: new BorderRadius.circular(5.0))),

    margin: new EdgeInsets.only(
    top: SizeConfig.safeBlockVertical * 10),
    ),
          new Container(
            height: SizeConfig.safeBlockVertical * 7, //10 for example
            width: SizeConfig.safeBlockHorizontal * 40,
            child:  new  RaisedButton ( color: Color.fromARGB(255, 0, 102, 204),elevation: 40,child:new Text(_local.home.nbj2, style: new TextStyle(
              color: Colors.white,

              fontSize: 20,),),onPressed: (){
    Navigator.of(context).push( new MaterialPageRoute(
    builder: (BuildContext context) =>
    new Conf(),
    ),   );
    },shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(5.0))),
            margin: new EdgeInsets.only(
                top: SizeConfig.safeBlockVertical * 5 ),
          ),
    new Container(
    height: SizeConfig.safeBlockVertical * 7, //10 for example
    width: SizeConfig.safeBlockHorizontal * 40,
    child:  new  RaisedButton ( color: Color.fromARGB(255, 0, 102, 204),elevation: 40,child:new Text(_local.home.nbj3, style: new TextStyle(
    color: Colors.white,

    fontSize: 20,),),onPressed: (){
      Navigator.of(context).push( new MaterialPageRoute(
        builder: (BuildContext context) =>
        new Conf(),
      ),   );
    },shape: new RoundedRectangleBorder(
    borderRadius: new BorderRadius.circular(5.0))),
    margin: new EdgeInsets.only(
    top: SizeConfig.safeBlockVertical * 5 ),
    ),

          new Container(
            height: SizeConfig.safeBlockVertical * 7, //10 for example
            width: SizeConfig.safeBlockHorizontal * 40,
            child:  new  RaisedButton ( color: Color.fromARGB(255, 0, 102, 204),elevation: 40,child:new Text(_local.home.nbj4, style: new TextStyle(
              color: Colors.white,

              fontSize: 20,),),onPressed: (){
              Navigator.of(context).push( new MaterialPageRoute(
                builder: (BuildContext context) =>
                new Conf(),
              ),   );
            },shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(5.0))),
            margin: new EdgeInsets.only(
                top: SizeConfig.safeBlockVertical * 5 ),
          ),


        ]),
    ),]),);

  }
}