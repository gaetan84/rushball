import 'package:flutter/material.dart';
import 'package:rushball/NbrdeJoueur.dart';
import 'package:rushball/config.dart';
import 'package:rushball/color.dart';
class Defilement extends StatefulWidget {
final String title;
final int valeur1;
Defilement({Key key, this.title,this.valeur1}): super (key:key);
  @override
  State<StatefulWidget> createState() {
    return _DefilementState();
  }
}
class _DefilementState extends State<Defilement> {
  final Key keyOne = PageStorageKey('pageOne');
  final Key keyTwo = PageStorageKey('pageTwo');

  int currentTab = 0;

  Conf one;
  Conf1 two;
  List<Widget> pages;
  Widget currentPage;

  List<Data> dataList;
  final PageStorageBucket bucket = PageStorageBucket();
  @override
  void initState() {
    dataList = [
Data(1, 1)

    ];
    one = Conf(
      key: keyOne,
      dataList: dataList,
    );
    two = Conf1(
      key: keyTwo,
    );

    pages = [one, two];

    currentPage = one;

    super.initState();
  }
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: PageStorage(
        child: currentPage,
        bucket: bucket,
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: currentTab,
        onTap: (int index) {
          setState(() {
            currentTab = index;
            currentPage = pages[index];
          });
        },
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text('Home'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            title: Text("Settings"),
          ),
        ],
      ),
    );
  }

}
class Data {
  final int id;

  final int value;
  Data(this.id, this.value);
}