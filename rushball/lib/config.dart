import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:rushball/color.dart';
import 'package:rushball/size.dart';
import 'package:rushball/NbrdeJoueur.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:rushball/Traduction/app_local.dart';
import 'package:rushball/Traduction/localisation.dart';
import 'package:rushball/Defilement.dart';
import 'package:rushball/verification/temps-socre.dart';



class Conf extends StatefulWidget{
  final List<Data> dataList;
  final String title;
const Conf ({Key key,this.dataList,this.title}): super (key:key);
  @override
  conf createState() => new conf();

}
class conf extends State<Conf> {

  List<String> t=['3', '6', '9', '12','15','18',];
int test1=0;
  int i=0;
String dropdownValue = '1';
String dropdownValue2;
String dropdownValue3 = '1';
String dropdownValue4 = '1';
int valeur=1;
int valeur1=1;
String temps;
String score;
  bool _value1 = true;
  bool _value2 = false;

  //we omitted the brackets '{}' and are using fat arrow '=>' instead, this is dart syntax
  void _value1Changed(bool value) { setState(() { _value1 = value;
  _value2=false;} );
}
  void _value2Changed(bool value) { setState(() { _value2 = value;
  _value1=false;} );
  }
  @override
  void initState ( ) {
    print (valeur1);
  }
    Widget build (BuildContext context){

    AppLocal _local= AppLocalizations.of(context).lang;
    return new Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
        body:

        SingleChildScrollView(


       child: new Center(

           child: new Column(
crossAxisAlignment: CrossAxisAlignment.center ,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  new Container(
                    child:
           new Row(
           mainAxisAlignment: MainAxisAlignment.center,
             children: <Widget>[


           new Text(_local.home.nbj1, style: new TextStyle(
             color: Colors.black,
             fontSize: 20,),),
          new Container(
              child:
                  new DropdownButton<String>(
                    key: new PageStorageKey<Type>(DropdownButton),
                      value: dropdownValue4,
                      items: <String>['1', '2', '3', '4'].map((String value) {
                        return new DropdownMenuItem<String>(

                          value: value,
                          child: new Text(value),
                        );
                      }).toList(),
                      onChanged: (String newValue) {

                        setState(() {
                          dropdownValue4 = newValue;
                          valeur= int.parse(dropdownValue4);

                        });}
                  ),
              margin: new EdgeInsets.only(
                  left: SizeConfig.safeBlockHorizontal * 10 )
          ),]),margin: new EdgeInsets.only(
                      top: SizeConfig.safeBlockHorizontal * 10 )),
                  new Container(

                 child: new Text(_local.home.conf1, style: new TextStyle(
                    color: Colors.black,
                    fontSize: 20,),),
                    margin: new EdgeInsets.only(
                      top: SizeConfig.safeBlockVertical * 1 ),),





                  SizedBox(
                    height: 150,
          child:
          new GridView.builder(
             padding: const  EdgeInsets.all(30),
             gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2,
           crossAxisSpacing: 50,

               childAspectRatio: 3/1,
           mainAxisSpacing: 1,),
            itemCount:valeur,
            itemBuilder: (BuildContext context, int i,)
           {

           return new TextField(
               onChanged: ( String teste ) {

               },
               decoration: InputDecoration(labelText: truc(i,context)),
           );
           }

           ),),
                  new Container( child: new Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[

                 new Text(_local.home.conf2, style: new TextStyle(
                      color: Colors.black,
                      fontSize: 20,),),
                    new Container(
                      child:
                    new DropdownButton<String>(
                      value: dropdownValue,
                      items: <String>['1','2','3', '4','5','6','7','8'].map((String value) {
                        return new DropdownMenuItem<String>(
                          value: value,
                          child: new Text(value),
                        );
                      }).toList(),
                      onChanged: (String newValue) {
    setState(() {
    dropdownValue = newValue;
    dropdownValue2=null;
    test1=int.parse(newValue);
    });}
                    ),
                        margin: new EdgeInsets.only(
                            left: SizeConfig.safeBlockHorizontal * 10 )),
                  ],), margin: new EdgeInsets.only(
            bottom: SizeConfig.safeBlockVertical * 1 )
            ),
                  new Container(
                      child: new Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[

                      new Text(_local.home.conf3, style: new TextStyle(
                        color: Colors.black,
                        fontSize: 20,),),

                      new Container(
                        child:
                      new DropdownButton<String>(
                          value: dropdownValue2,
                          items: test(test1).map((String value) {
                            return new DropdownMenuItem<String>(
                              value: value,
                              child: new Text(value),
                            );
                          }).toList(),
                          onChanged: (String newValue) {
                            setState(() {

                              dropdownValue2 = newValue;
                            });}
                      ), margin: new EdgeInsets.only(
                          left: SizeConfig.safeBlockHorizontal * 10 ) ),
                    ],), margin: new EdgeInsets.only(
                      bottom: SizeConfig.safeBlockVertical * 1 )
                  ),


                 new Column(
                        children: <Widget>[
                          new Row (children: <Widget>[
new Container (

                     width: 220,
  child:
                          new CheckboxListTile(
                            value: _value1,
                            onChanged: _value1Changed,
                            title: new Text('Limite de temps'),
                            controlAffinity: ListTileControlAffinity.leading,


                            activeColor: Colors.red,
                          ),),
                          new  Visibility(child:
                            new Row(
                              children: <Widget>[
                                new  Container(
                                  height:SizeConfig.safeBlockVertical * 10 ,
                                  width:SizeConfig.safeBlockHorizontal * 20 ,
                                  child:
                                  new TextField(   onChanged: ( String teste ) {
                                    setState(() {
                                      temps=teste;
                                    });
                                  },decoration: InputDecoration(labelText: "Temps en Seconde"),keyboardType: TextInputType.numberWithOptions(
                                    signed: false,
                                    decimal: false,
                                  ),
                                    maxLength: 3,),),

                                new Text('Seconde',style: new TextStyle(

                                  fontSize: 20,)),
                              ],
                            ),
                              visible: _value1,),]),
                          new Row (children: <Widget>[
                          new Container(  height: 50,
                              width: 200,child:
                          new CheckboxListTile(
                            value: _value2,
                            onChanged: _value2Changed,
                            title: new Text('Limite de score'),
                            controlAffinity: ListTileControlAffinity.leading,


                            activeColor: Colors.red,
                          )
                          ),Visibility(child:
                            new Row(
                              children: <Widget>[
                                new  Container(
                                  height:SizeConfig.safeBlockVertical * 10 ,
                                  width:SizeConfig.safeBlockHorizontal * 20 ,
                                  child:
                                  new TextField(
                                    onChanged: ( String teste ) {
                                      setState(() {
                                        score=teste;
                                      });
                                    },
                                    decoration: InputDecoration(labelText: "Score maximal"),keyboardType: TextInputType.numberWithOptions(
                                    signed: false,
                                    decimal: false,
                                  ),
                                    maxLength: 3,),),

                                new Text('points',style: new TextStyle(

                                  fontSize: 20,)),
                              ],
                            ),
                              visible: _value2,),]),
                        ],
                      ),
new

                  Container(
                    height: SizeConfig.safeBlockVertical * 7, //10 for example
                    width: SizeConfig.safeBlockHorizontal * 40,
                    child:  new  RaisedButton ( color: Color.fromARGB(255, 0, 102, 204),elevation: 0,child:new Text(_local.home.nbj3, style: new TextStyle(
                      color: Colors.white,

                      fontSize: 20,),),onPressed: (){

                      if (Verifbool(temps, score, _value1)==true){
                     String  Texte= VerifScore_Temps(temps, score, _value1);
                        showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            // return object of type Dialog
                            return AlertDialog(
                              title: new Text(Texte),
                              content: new Text("g"),
                              actions: <Widget>[
                                // usually buttons at the bottom of the dialog
                                new FlatButton(
                                  child: new Text("g"),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                ),
                              ],
                            );
                          },
                        );
                      } else
                      Navigator.of(context).push( new MaterialPageRoute(
                        builder: (BuildContext context) =>
                        new Conf1(title: widget.title,),
                      ),   );
                    },shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(5.0))),
                    margin: new EdgeInsets.only(
                        top: SizeConfig.safeBlockVertical * 15),
                  ),

      ]),),),
    );
  }
  }
String truc (int i,context){

  AppLocal _local= AppLocalizations.of(context).lang;
String text;
  switch (i){

    case 0:
      {
        text=_local.home.nbj5;
      }
      break;
    case 1:
      {
        text=_local.home.nbj6;
      }
      break;
    case 2:
      {
        text=_local.home.nbj7;
      }
      break;
    case 3:
      {
        text=_local.home.nbj8;
      }

  }
  return (text);
}

List <String>test (int i,){
switch(i){

  case 1:
    {
      List<String> t = ['1', '2',];
     return t;
    }
    break;
  case 2:
    {
      List<String> t = ['1', '2', '3',];
      return t;
    }
    break;
  case 3:
    {
      List<String> t = ['1', '2', '3','4','5'];
      return t;
    }
    break;
  case 4:
    {
      List<String> t = ['2', '4', '6',];
      return t;
    }
    break;
  case 5:
    {
      List<String> t = ['2', '4', '6','8'];
      return t;
    }
    break;
  case 6:
    {
      List<String> t = ['2', '4', '6','8','10'];
      return t;
    }
    break;
  case 7:
    {
      List<String> t = ['2', '4', '6','8','10','12'];
      return t;
    }
    break;
  case 8:
    {
      List<String> t = ['2', '4', '6','8','10','12','14'];
      return t;
    }
    break;
  default:
    {
      List<String> t=['3', '6', '9', '12','15','18',];
      return t;
    }
}


}
drow(int i){

  switch(i){

    case 1:{
      String drow;
      return drow;
    }break;
    case 2:{
      String drow;
      return drow;
    }break;
    case 3:{
      String drow;
      return drow;
    }break;
    case 4:{
      String drow;
      return drow;
    }break;
    case 5:{
      String drow;
      return drow;
    }break;
    case 6:{
      String drow;
      return drow;
    }break;
    case 7:{
      String drow;
      return drow;
    }break;
    case 8:{
      String drow;
      return drow;
    }break;
  }
}