
import 'package:rushball/Traduction/app_local.dart';

class EnLocal extends AppLocal {
@override
HomeLocal get home => HomeLocal(
  wifi: 'WIFI',
  bluetooh: 'Bluetooh',
  mode1: 'Score limite',
  mode2: 'Time limite',
  mode3:  'Shoot limited',
  mode4: 'Survival',
  nbj1:  'Choice the number of player',
  nbj2:  '2 Players',
  nbj3:  '3 Players',
  nbj4:  '4 Players',
  nbj5: 'player 1',
  nbj6: 'player 2',
  nbj7: 'player 3',
  nbj8: 'player 4',
  nbj9: '1 Player',
  conf1: 'Name of players',
  conf2: 'Number of target panels',
  conf3: 'Number of target alight',
  conf4: 'Number of  color',
  conf5: '(Joker no understood)',
  color: 'Bleu clair',
  color1: 'Point',
  color2: 'yes',
  color3: 'no',
  color4: 'Joker activate',
  color5: 'Vert',
  color6: 'Orange',

);
}