import 'package:rushball/Traduction/en.dart';
import 'package:rushball/Traduction/fr.dart';
import 'package:rushball/Traduction/app_local.dart';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart'show SynchronousFuture;



class AppLocalizations {
  final Locale locale;

  Map<String, AppLocal> _localizedValues = {
    'fr': FrLocal(),
    'en': EnLocal(),


  };

  AppLocalizations(this.locale);

  static AppLocalizations of(BuildContext context) =>
      Localizations.of<AppLocalizations>(context, AppLocalizations);

  AppLocal get lang => _localizedValues[locale.languageCode];
}
class AppLocalizationsDelegate extends LocalizationsDelegate<AppLocalizations> {
  const AppLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) => true;

  @override
  Future<AppLocalizations> load(Locale locale) =>
      SynchronousFuture<AppLocalizations>(AppLocalizations(locale));

  @override
  bool shouldReload(AppLocalizationsDelegate old) => false;
}