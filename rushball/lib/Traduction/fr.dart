import 'package:rushball/Traduction/app_local.dart';

class FrLocal extends AppLocal {
@override
HomeLocal get home => HomeLocal(
    wifi: 'WIFI',
    bluetooh: 'Bluetooh',
    mode1: 'Limite de Score',
    mode2: 'Temps limité',
    mode3:  'Limite de tir',
    mode4: 'Survivre',
    nbj1:  'Choisir le nombre de joueur',
    nbj2:  '2 Joueurs',
    nbj3:  '3 Joueurs',
    nbj4:  '4 Joueurs',
    nbj5: 'joueur 1',
    nbj6: 'joueur 2',
    nbj7: 'joueur 3',
    nbj8: 'joueur 4',
    nbj9: '1 Joueurs',
    conf1: 'Nom des joueurs',
    conf2: 'Nombre de panneaux de cibles',
    conf3: 'Nombre de cibles allumées',
    conf4: 'Nombre de couleurs',
    conf5: '(Joker non compris)',
    color: 'point',
    color1: 'point',
    color2: 'oui',
    color3: 'non',
    color4: 'Joker activé',
    color5: 'Vert',
    color6: 'Orange',







);

}
