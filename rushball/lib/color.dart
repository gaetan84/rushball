import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:rushball/size.dart';
import 'package:rushball/NbrdeJoueur.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:rushball/Traduction/app_local.dart';
import 'package:rushball/Traduction/localisation.dart';
import 'package:rushball/Defilement.dart';
import 'package:rushball/verification/couleur.dart';
import 'package:rushball/verification/points.dart';
import 'package:string_validator/string_validator.dart';
import 'package:rushball/verification/joker.dart';
import 'package:slider_button/slider_button.dart';
class Conf1 extends StatefulWidget{


  final String title;



  Conf1 ({Key key,this.title,}): super (key:key);
  @override
  conf1 createState() => new conf1();
}
class conf1 extends State<Conf1> {
  bool _value1 = false;
  bool _value2 = false;
  void _onChanged1(bool value) => setState(() => _value1 = value);
  void _onChanged2(bool value) => setState(() => _value2 = value);
  final PageStorageBucket _bucket = new PageStorageBucket();
  PageStorageKey mykey = new PageStorageKey("testkey");
  int i=0;
  int val=0;
  int valeur1=6;
  String dropdownValue = '4';
  String dropdownValue2 = '3';
  String dropdownValue3 = '6';
  String dropdownValue4 = '1';
  int valeur=1;
  bool joker= false;
  bool joker1= false;
  String Texte='';
  String Texte2='';
  String Texte3='';
  String Joker;
  List<ColorModel>_colors=[
    ColorModel(color: Colors.blue, ),
    ColorModel(color: Colors.cyan,),
    ColorModel(color: Colors.white, ),
    ColorModel(color: Colors.purpleAccent, ),
    ColorModel(color: Colors.red,),
    ColorModel(color: Colors.green, ),

  ];
  Color _selectedColor = Colors.purpleAccent;
  Color _selectedColor1 = Colors.green;
  Color _selectedColor2 = Colors.red;
  Color _selectedColor3 = Colors.blue;
  Color _selectedColor4 = Colors.cyan;
  Color _selectedColor5 = Colors.white;
  String _Point1;
  String _Point2;
  String _Point3;
  String _Point4;
  String _Point5;
  String _Point6;
  @override
  void initState ( ) {


    print(Data(1, 5));


  }


  Widget build (BuildContext context){

    AppLocal _local= AppLocalizations.of(context).lang;



    return PageStorage(child: new Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body:
      SingleChildScrollView(


        child: new Center(

          child: new Column(
              crossAxisAlignment: CrossAxisAlignment.center ,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[

                new Container(
                  height:80,
          child: new Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[

                            new Text(_local.home.conf4, style: new TextStyle(
                              color: Colors.black,
                              fontSize: 20,),),
                            new Text(_local.home.conf5, style: new TextStyle(
                              color: Colors.black,
                              fontSize: 15,),)]),
                      new Container(
                          child:
                          new DropdownButton<String>(
                              value: dropdownValue3,
                              items: <String>['1', '2', '3', '4','5','6'].map((String value) {
                                return new DropdownMenuItem<String>(
                                  value: value,
                                  child: new Text(value),
                                );
                              }).toList(),
                              onChanged: (String newValue) {
                                setState(() {
                                  dropdownValue3 = newValue;
                                  valeur1=int.parse(dropdownValue3);


                                });}
                          ),   margin: new EdgeInsets.only(
                          left: SizeConfig.safeBlockHorizontal * 10 ) ),

                    ],),
                ),
                new Container(
height: 40  ,
                  child: new Text(_local.home.conf1, style: new TextStyle(
                    color: Colors.black,
                    fontSize: 20,),),
                  margin: new EdgeInsets.only(
                      top: SizeConfig.safeBlockVertical * 1,bottom: SizeConfig.safeBlockVertical * 5  ),),





                SizedBox(
                  height: 270,
                    child:
                  new GridView.builder(
                      padding: const  EdgeInsets.all(0),

                      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3,
                        crossAxisSpacing: 2,

                        childAspectRatio: 1/1,
                        mainAxisSpacing: 1,),
                      itemCount: valeur1,
                      itemBuilder: (BuildContext context, int i,)

                      {

                        return new Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Expanded(child:
                            Container(
                              width:84 ,
                              height: 50,
                              color: couleur(i,_selectedColor,_selectedColor1,_selectedColor2,_selectedColor3,_selectedColor4,_selectedColor5),
                              child: DropdownButton<Color>(
                                items: _colors
                                    .map((color) => DropdownMenuItem<Color>(
                                  child: Container(
                                    color: color.color,
                                  ),
                                  value: color.color,
                                ))
                                    .toList(),
                                onChanged: (Color value) {
                                  switch (i) {
                                    case 0:
                                      {
                                        setState(() =>  _selectedColor=value );
                                      }
                                      break;
                                    case 1:
                                      {
                                        setState(() =>  _selectedColor1=value );
                                      }
                                      break;
                                    case 2:

                                      {
                                        setState(() =>  _selectedColor2=value );
                                      }
                                      break;
                                    case 3:
                                      {
                                        setState(() =>  _selectedColor3=value );
                                      }
                                      break;
                                    case 4:
                                      {
                                        setState(() =>  _selectedColor4=value );
                                      }
                                      break;

                                    case 5:
                                      {
                                        setState(() =>  _selectedColor5=value );
                                      }
                                      break;


                                  }

                                },

                              ),
                            ),),
                            Container(
                              height:84,
                              width:84,
                              child:
                              new TextField(

                                onChanged: ( String value ) {

                                  switch (i) {
                                    case 0:
                                      {
                                        setState(() =>  _Point1=value );
                                      }
                                      break;
                                    case 1:
                                      {
                                        setState(() =>  _Point2=value );
                                      }
                                      break;
                                    case 2:

                                      {
                                        setState(() =>  _Point3=value );
                                      }
                                      break;
                                    case 3:
                                      {
                                        setState(() =>  _Point4=value );
                                      }
                                      break;
                                    case 4:
                                      {
                                        setState(() =>  _Point5=value );
                                      }
                                      break;
                                    case 5:
                                      {
                                        setState(() =>  _Point6=value );
                                      }
                                      break;
                                  }
                                },
                                decoration: InputDecoration(labelText: _local.home.color1),
                                keyboardType: TextInputType.numberWithOptions(
                                  signed: false,
                                  decimal: false,
                                ),
                                maxLength: 2,
                              ),),
                          ],
                        );





                      }

                  ),),
                new Container(
                  height: 60,
                    child: new Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[

                    new Text(_local.home.color4, style: new TextStyle(
                      color: Colors.black,
                      fontSize: 20,),),



                  Switch(
                      value: joker1, onChanged
                        : (bool value) {
                      setState (() {
                        joker1 = value;
                        joker=false;
                        if (joker1==true){ val=1;}else{
                          val=0;
                        }
                        return val;
                      });
                    },
                    ),


                  ],), margin: new EdgeInsets.only(
                    bottom: SizeConfig.safeBlockVertical * 0 )
                ),

        Visibility(
            child:new Container(
              height: 60  ,
              child:

                        new Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[

                            Container(
                              width:SizeConfig.safeBlockHorizontal *20 ,
                              height: SizeConfig.blockSizeVertical*5,
                              color: Colors.yellow,

                            ),
                            Container(
                                width:SizeConfig.safeBlockHorizontal * 20 ,
                                child:
                                new TextField(

                                  onChanged: ( String teste ) {
setState(() {
  Joker=teste;
});
                                  },
                                  decoration: InputDecoration(labelText: _local.home.color1),
                                  keyboardType: TextInputType.numberWithOptions(
                                    signed: false,
                                    decimal: false,
                                  ),
                                  maxLength: 3,
                                ),
                                margin: new EdgeInsets.only(
                                    left: SizeConfig.safeBlockVertical * 4 )),

                          ],
                        ),  ), visible: joker1, ),




                new Container(
                  height: 50, //10 for example
                  width: 150,
                  child:  new  RaisedButton ( color: Color.fromARGB(255, 0, 102, 204),elevation: 0,child:new Text(_local.home.nbj3, style: new TextStyle(
                    color: Colors.white,

                    fontSize: 20,),),onPressed: (){
                    print (_Point1);

                    Texte= Verifcouluer(_selectedColor,_selectedColor1,_selectedColor2,_selectedColor3,_selectedColor4,_selectedColor5,context,valeur1);
                    Texte2=Verifpoints(_Point1, _Point2, _Point3, _Point4,_Point5,_Point6, context,valeur1);
                   Texte3=VerifJoker(Joker);
                    showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        // return object of type Dialog
                        return AlertDialog(
                          title: new Text(Texte),
                          content: new Text(Texte2),
                          actions: <Widget>[
                            // usually buttons at the bottom of the dialog
                            new FlatButton(
                              child: new Text("g"),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                            ),
                          ],
                        );
                      },
                    );
                  },shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(5.0))),
                  margin: new EdgeInsets.only(
                      top: SizeConfig.safeBlockVertical * 1),
                )

              ]),),),
    ),    bucket: _bucket,
      key: mykey, );
  }
}
String truc (int i,context){

  AppLocal _local= AppLocalizations.of(context).lang;
  String text;
  switch (i){

    case 0:
      {
        text=_local.home.nbj5;
      }
      break;
    case 1:
      {
        text=_local.home.nbj6;
      }
      break;
    case 2:
      {
        text=_local.home.nbj7;
      }
      break;
    case 3:
      {
        text=_local.home.nbj8;
      }

  }
  return (text);
}
//Create a Model class to hold key-value pair data
class ColorModel {
  String colorName;
  Color color;

  ColorModel({this.colorName, this.color});
}

couleur (int i,var ja,var f,var fe,var fr,var coul,var cou){


  switch (i) {
    case 0:
      {
        return ja;
      }
      break;
    case 1:
      {
        return f;
      }
      break;
    case 2:

      {
        return fe;
      }
      break;
    case 3:
      {
        return fr;
      }
      break;
    case 4:
      {
        return coul;
      }
      break;
    case 5:
      {
        return cou;
      }
      break;

  }
}