import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:rushball/size.dart';
import 'package:rushball/Mode de jeu.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:rushball/Traduction/app_local.dart';
import 'package:rushball/Traduction/localisation.dart';
class Home extends StatefulWidget{
  @override
  State<StatefulWidget>createState(){
    return _HomeState();
  }
}
class _HomeState extends State<Home>{

  @override

  Widget build(BuildContext context) {
    AppLocal _local = AppLocalizations.of(context).lang;

    SizeConfig().init(context); 100;
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
        title: Image.asset("images/image bidule.png",fit: BoxFit.cover,
          alignment: Alignment (
              0, -0.9 ),
        ),
    centerTitle: true,
    ),
    body:MaterialApp(

      debugShowCheckedModeBanner: false,

      home : new Scaffold(

        body: ListView(
          children: <Widget> [
       
        new Container(
        child: new Column(

            mainAxisAlignment: MainAxisAlignment.center,

            children: <Widget>[
            new Container(
              height: SizeConfig.safeBlockVertical * 7, //10 for example
              width: SizeConfig.safeBlockHorizontal * 40,
              child:  new  RaisedButton ( color: Color.fromARGB(255, 0, 102, 204),elevation: 40, child:new Text(_local.home.wifi, style: new TextStyle(
                color: Colors.white,
                fontSize: 20,),),onPressed: (){
    Navigator.push(context, new MaterialPageRoute(builder: (BuildContext context){
    return new Modedejeu();
    }));},shape: new RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(5.0))),

              margin: new EdgeInsets.only(
                  top: SizeConfig.safeBlockVertical * 25 ),
            ),
              new Container(
                height: SizeConfig.safeBlockVertical * 7, //10 for example
                width: SizeConfig.safeBlockHorizontal * 40,
                child:  new  RaisedButton ( color: Color.fromARGB(255, 0, 102, 204),elevation: 40,child:new Text(_local.home.bluetooh, style: new TextStyle(
                  color: Colors.white,

                  fontSize: 20,),),onPressed: (){},shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(5.0))),
                margin: new EdgeInsets.only(
                    top: SizeConfig.safeBlockVertical * 5 ),
              ),]),
    ),]),),
    ),  );
  }


}
